import os, sys, time, logging, json, datetime, pytz, csv
from pytz import utc
from shutil import copy
from data.history import FILENAME_BITSTAMP_HISTORY, DIRECTORY_HISTORY

HEADER_ROW = ['unixtime', 'price', 'amount', 'type']
DIRECTORY_LIVEFEED = 'feed'
#FILENAME_BITSTAMP_HISTORY = 'bitstamp_usd_history_all_ticks.csv'

############################################################

def _write_csv(f_name, row):
    with open(f_name,'a') as f:
        csv.writer(f).writerow(row)

def save_feed(logger, data_dict):
    utc_time = utc.localize(datetime.datetime.utcfromtimestamp(int(data_dict['timestamp'])))
    time_est = utc_time.astimezone(pytz.timezone('US/Eastern'))
    time_est_str = time_est.strftime('%Y-%m-%d_%H') # bucket for each hour
    logger.info('utc: %s' % str(utc_time))
    logger.info('time_est: %s' % str(time_est))
    logger.info('time_est_str: %s' % str(time_est_str))
    if not os.path.exists(DIRECTORY_LIVEFEED):
        logger.info('Creating %s' % DIRECTORY_LIVEFEED)
        os.makedirs(DIRECTORY_LIVEFEED)

    # write to csv with header row
    f_name = os.path.join(DIRECTORY_LIVEFEED, time_est_str)
    if not os.path.exists(f_name): _write_csv(f_name, HEADER_ROW)
    _write_csv(f_name, [data_dict['timestamp'], data_dict['price'], data_dict['amount'], data_dict['type']])

def _feed_to_history(logger, feed):
    logger.info('processing the feed %s.' % feed)    
    history = os.path.join(DIRECTORY_HISTORY, FILENAME_BITSTAMP_HISTORY)

    # skip if the feed is already in the history
    with open(history, "r") as his_f:
        last_row = his_f.readlines()[-1]
        last_timestamp = int(last_row.split(',')[0])

    rows = csv.reader(open(feed, 'r'), delimiter=',', quotechar='|')
    header = True
    first_row = True
    for row in rows:
        if header:
            header = False
        else:
            if first_row:
                first_row = False
                if int(row[0]) <= last_timestamp:
                    logger.info('the feed %s is already in the history.' % feed)
                    return

            _write_csv(history, row)

def feeds_to_history(logger):
    # bkup the history
    history = os.path.join(DIRECTORY_HISTORY, FILENAME_BITSTAMP_HISTORY)
    name, ext = os.path.splitext(history)
    copy(history, name + '_bkup' + ext)

    # update the history with feeds
    feeds = sorted(os.listdir(DIRECTORY_LIVEFEED))
    # skip the last feed as it is still being updated
    for feed in feeds[:-1]:
         _feed_to_history(logger, os.path.join(DIRECTORY_LIVEFEED, feed))
