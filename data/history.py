import os, boto3, progressbar

DIRECTORY_HISTORY = 'datafiles'
FILENAME_BITSTAMP_HISTORY = 'bitstamp_usd_history_all_ticks.csv'
FILENAME_BITSTAMP_1M_TICKS = 'bitstamp_usd_history_1m.csv'
BUCKET_HISTORY_BITSTAMP = 'bitcoin-price-headgecoast'

history = FILENAME_BITSTAMP_1M_TICKS
downloaded = None
history_size = os.stat(os.path.join(DIRECTORY_HISTORY, history)).st_size

bar = progressbar.ProgressBar()
def _upload_hook(size):
    global downloaded
    downloaded += size
    bar.update(min(100, 100.0 * downloaded / history_size))

def history_to_s3():
    global downloaded
    downloaded = 0
    client = boto3.client('s3')
    BUCKET_HISTORY_BITSTAMP

    path = os.path.join(DIRECTORY_HISTORY, history)
    client.upload_file(path, BUCKET_HISTORY_BITSTAMP, history, Callback = _upload_hook)
    bar.update(100)

if __name__ == '__main__':
    history_to_s3()
