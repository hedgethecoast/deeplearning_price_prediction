import pandas as pd, numpy as np, matplotlib.pyplot as plt, datetime

def load_data():
    data_day = pd.read_csv("datafiles/bitstamp_usd_daily.csv", index_col = 0)
    data_day = data_day.iloc[::-1]
    print(data_day.tail(3))

    data_tick = pd.read_csv("datafiles/bitstamp_usd_history_1m.csv", index_col = 0)
    data_tick.index = data_tick.index.values.astype("datetime64[s]").astype('<M8[m]')
    # minute resolution data has problem: it almost always increases, so the model does not learn anything.
    data_minute = pd.DataFrame(index=data_tick.index.unique())
    data_tick['weight'] = data_tick['amount'] / data_tick.groupby(data_tick.index)['amount'].sum()
    data_tick['wprice'] = data_tick['price'] * data_tick['weight']
    data_minute['volume'] = data_tick.groupby(data_tick.index)['amount'].sum()
    data_minute['vwap'] = data_tick.groupby(data_tick.index)['wprice'].sum()
    print(data_tick.tail(3))
    print(data_minute.tail(3))
    return data_tick, data_minute, data_day

##########################################################

# how many past prices to consider
look_back = 4 # 80

# the number of future data to label over
expect_window = 2

def decorate(history, price_column_name = 'VWAP', volume_column_name = None, cut_tail_expect_window = True):
    decorated = pd.DataFrame(index=history.index)
    decorated.loc[:,'price'] = history.loc[:,price_column_name]
    decorated.loc[:,'diff'] = history.loc[:,price_column_name].diff()
    decorated.loc[:,'diff_ratio'] = decorated.loc[:,'diff'] / history.loc[:,price_column_name]
    decorated.loc[:,'abs_diff_ratio'] = abs(decorated.loc[:,'diff_ratio'])
    if not volume_column_name is None:
        decorated.loc[:,'volume'] = history.loc[:,volume_column_name]
        decorated.loc[:,'prev_volume'] = decorated.loc[:,'volume'].shift(1)
    decorated.loc[:,'future_mean'] = history.iloc[::-1].rolling(expect_window).mean().iloc[::-1].loc[:,price_column_name]
    decorated.loc[:,'future_mean_diff_ratio'] = (decorated.loc[:,'future_mean'] - history.loc[:,price_column_name]) / history.loc[:,price_column_name]
    decorated.loc[:,'future_diff_ratio'] = (history.loc[:,price_column_name].shift(-expect_window) - history.loc[:,price_column_name]) / history.loc[:,price_column_name]
    return decorated[:-expect_window] if cut_tail_expect_window else decorated

def get_decorated(type = 'tick'):
    data_tick, data_minute, data_day = load_data()
    decorated = None
    if type == 'tick':
        decorated = decorate(data_tick, price_column_name = 'price', volume_column_name = 'amount')
    elif type == 'minute':
        decorated = decorate(data_minute, price_column_name = 'vwap', volume_column_name = 'volume')
    elif type == 'day':
        decorated = decorate(data_day, price_column_name = 'VWAP', volume_column_name = 'Volume')
    return decorated

##########################################################

label_threshold = 0.005

def format(decorated):
    data = pd.DataFrame(index=decorated.index)
    #data.loc[:,'p0'] = decorated.loc[:,'price']
    for i in range(look_back):
        data.loc[:,'p' + str(i+1)] = (decorated.loc[:,'price'].shift(i+1) - decorated.loc[:,'price'])
    #data.loc[:,'volume_sum'] = decorated.loc[:,'volume'].rolling(expect_window).sum()
    
    labels = pd.DataFrame(index=decorated.index, dtype=np.float32)
    '''
    labels['up'] = np.where(decorated['future_mean_diff_ratio'] > label_threshold, 1.0, 0)
    labels['no_move'] = np.where(decorated['future_mean_diff_ratio'] > -label_threshold, 1.0, 0) - labels['up']
    labels['down'] = np.where(decorated['future_mean_diff_ratio'] < -label_threshold, 1.0, 0)
    '''
    labels['up'] = np.where(decorated['future_diff_ratio'] >= 0, 1.0, 0)
    labels['down'] = np.where(decorated['future_diff_ratio'] < 0, 1.0, 0)
    
    return data[look_back:], labels[look_back:], data[look_back:].index

##########################################################

TEST_SIZE_NN = 1000
TAIL_VALID_SIZE_NN = 10000
TRAIN_SZIE_NN = 800000 # 100k ~ 17 days
HEAD_VALID_SIZE_NN = 50000

TEST_SIZE_LSTM = 1000
TAIL_VALID_SIZE_LSTM = 10000
TRAIN_SZIE_LSTM = 100000 # 200k causes Out Of Memory
HEAD_VALID_SIZE_LSTM = 20000

def get_data(test_size, tail_valid_size, head_valid_siz, train_size):
    decorated = get_decorated()

    test_subset = test_size + look_back + expect_window
    tail_valid_subset_i = tail_valid_size + test_subset
    train_subset_i = train_size + tail_valid_subset_i
    head_valid_subset_i = head_valid_siz + train_subset_i

    test_data, test_labels, test_index = format(decorated[-test_subset:])
    tail_valid_data, tail_valid_labels, tv_index = format(decorated[-tail_valid_subset_i:-test_subset])
    train_data, train_labels, train_index = format(decorated[-train_subset_i:-tail_valid_subset_i])
    head_valid_data, head_valid_labels, hv_index = format(decorated[-head_valid_subset_i:-train_subset_i])

    print('test', np.sum(test_labels, 0))
    print('tail valid', np.sum(tail_valid_labels, 0))
    print('head valid', np.sum(head_valid_labels, 0))
    print('train', np.sum(train_labels, 0))

    return test_data, test_labels, test_index, tail_valid_data, tail_valid_labels, tv_index, head_valid_data, head_valid_labels, hv_index, train_data, train_labels, train_index





