import pandas as pd, numpy as np, matplotlib.pyplot as plt, datetime
from data import read
from models.manager import predict, load

data_tick, _, _ = read.load_data()
decorated = read.decorate(pd.DataFrame({'price': range(read.look_back + 2)}), price_column_name = 'price', cut_tail_expect_window = False)
formatted = read.format(decorated)
print(formatted)
print(formatted[-1])
price_formatted = formatted[0].iloc[-1]
price_reshaped = price_formatted.reshape(1, price_formatted.shape[0])

load()
pred = predict(price_reshaped)
print(pred)