import tensorflow as tf, math, pandas as pd, numpy as np
import matplotlib.pyplot as plt
from data.read import get_data, look_back, TEST_SIZE_NN, TAIL_VALID_SIZE_NN, TRAIN_SZIE_NN, HEAD_VALID_SIZE_NN, decorated

##########################################################

print('[process data]')
test_data, test_labels, test_index, tail_valid_data, tail_valid_labels, tv_index, head_valid_data, head_valid_labels, \
    hv_index, train_data, train_labels, train_index = get_data(TEST_SIZE_NN, TAIL_VALID_SIZE_NN, HEAD_VALID_SIZE_NN, TRAIN_SZIE_NN)
print('[/process data]')

##########################################################

print('[plot]')

def plot(time, data1, data2, l1 = '1', l2 = '', c1 = 'r', c2 = 'b'):
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()

    ax1.plot(time, data1, color=c1)
    ax1.set_xlabel('time')
    ax1.set_ylabel(l1)

    ax2.plot(time, data2, color=c2)
    ax2.set_ylabel(l2)
    return ax1, ax2

pred_train = pd.DataFrame.from_csv('predictions/pred_train.csv')
jn = decorated.join(pred_train, how='inner')
jnst = jn[-4000:]

plot(jnst.index, jnst['price'], jnst['up'])
plt.show()

print('[/plot]')