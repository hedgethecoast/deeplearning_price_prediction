import os, sys, time, logging, json, pusherclient
from data import feed, history

APP_KEY_BITSTAMP = 'de504dc5763aeef9ff52'
CHANNEL_TICKER = 'live_trades'
CHANNEL_ORDER_BOOK = 'order_book'
EVENT_TICKER = 'trade'
EVENT_DATA = 'data'

# Add a logging handler so we can see the raw communication data
logger = logging.getLogger()
logger.setLevel(logging.INFO)
# sends the log to stdout
logger.addHandler(logging.StreamHandler(sys.stdout))

def on_ticker(data):
    logger.info('on_ticker')
    pairs = json.loads(data)
    feed.save_feed(logger, pairs)

def orderbook(data):
    pairs = json.loads(data)
    bids, asks = pairs['bids'], pairs['asks']
    print('10 bids')
    for p, v in bids[:10]:
        print(p, v)
    print('10 asks')
    for p, v in asks[:10]:
        print(p, v)

# We can't subscribe until we've connected, so we use a callback handler
# to subscribe when able
global pusher
def on_connection(data):
    pusher.subscribe(CHANNEL_TICKER).bind(EVENT_TICKER, on_ticker)
    #pusher.subscribe(CHANNEL_ORDER_BOOK).bind(EVENT_DATA, orderbook)

pusher = pusherclient.Pusher(APP_KEY_BITSTAMP)
pusher.connection.bind('pusher:connection_established', on_connection)
pusher.connect()

while True:
    # Do other things in the meantime here...
    time.sleep(60 * 60 * 6) # by second
    feed.feeds_to_history(logger);
    history.history_to_s3()
