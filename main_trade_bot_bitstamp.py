import os, sys, time, logging, json, pusher, pusherclient, pandas as pd
from collections import deque
from data.read import decorate, look_back, expect_window, format as doformat
from models.manager import load, predict, on_tick

APP_KEY_BITSTAMP = 'de504dc5763aeef9ff52'
CHANNEL_TICKER = 'live_trades'
CHANNEL_ORDER_BOOK = 'order_book'
EVENT_TICKER = 'trade'
EVENT_DATA = 'data'

pusher_broadcast = pusher.Pusher(
  app_id='361388',
  key='5e2df9ea5a67f152b7e3',
  secret='f5e7f91523843d383083',
  cluster='us2',
  ssl=True
)

# Add a logging handler so we can see the raw communication data
logger = logging.getLogger()
logger.setLevel(logging.INFO)
# sends the log to stdout
logger.addHandler(logging.StreamHandler(sys.stdout))

load()
def on_ticker(data):
    logger.info('on_ticker')
    data_dict = json.loads(data)
    price = data_dict['price']
    on_tick(price)
    pred = predict()
    print(pred)
    if pred[0][0] > 0.7:
        print('buy at %s' % price)
    elif pred[0][1] > 0.7:
        print('sell at %s' % price)
        #pusher_client.trigger('bitcoin_training', 'on_ticker', {'message': 'hello world'})

# We can't subscribe until we've connected, so we use a callback handler to subscribe when able
global pusher_client
def on_connection(data):
    pusher_client.subscribe(CHANNEL_TICKER).bind(EVENT_TICKER, on_ticker)

pusher_client = pusherclient.Pusher(APP_KEY_BITSTAMP)
pusher_client.connection.bind('pusher:connection_established', on_connection)
pusher_client.connect()

while True:
    # Do other things in the meantime here...
    time.sleep(60 * 60 * 6) # by second
    feed.feeds_to_history(logger);
    history.history_to_s3()
